﻿using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for GuestAppointments.xaml
    /// </summary>
    public partial class GuestAppointments : Window
    {
        ICollectionView view;
        String selectedFilter = "Active";
        public GuestAppointments()
        {
            InitializeComponent();
            InitializeView();
        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Appointments);
            dgAppointments.ItemsSource = view;
        }

        private void dgAppointments_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error")
                || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            GuestWindow g = new GuestWindow();
            g.Show();
            this.Close();
        }
    }
}
