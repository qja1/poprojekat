﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for InstitutionForm.xaml
    /// </summary>
    public partial class InstitutionForm : Window
    {
        enum Status { ADD, EDIT }
        private Status _status;
        private Institution selectedInstitution;

        public InstitutionForm(Institution institution)
        {
            InitializeComponent();
            if (institution.InsName.Equals(""))
            {
                this._status = Status.ADD;
            }
            else
            {
                this._status = Status.EDIT;  
            }
            selectedInstitution = institution;
            this.DataContext = institution;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Institution ins = Data.GetInstitutionByName(txtName.Text);
            if (ins != null && _status.Equals(Status.ADD))
            {
                MessageBox.Show($"Institution with name {ins.InsName} exists!", "Warning", MessageBoxButton.OK);
                return;
            }
            Institution ins2;

            ins2 = new Institution(selectedInstitution.InsName, selectedInstitution.InsAdress, selectedInstitution.Active, null);

            if (_status.Equals(Status.ADD))
            {
                int idIns = ins2.SaveInstitution();
                ins2.InsId = idIns;
                Data.Institutions.Add(ins2);
            }
            if (_status.Equals(Status.EDIT))

                ins2.UpdateInstitution();

            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
