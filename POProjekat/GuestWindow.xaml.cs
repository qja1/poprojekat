﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for GuestWindow.xaml
    /// </summary>
    public partial class GuestWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "Active";

        public GuestWindow()
        {
            InitializeComponent();
            InitializeView();
            view.Filter = CustomFilter;
            cmbSearchIns.ItemsSource = new List<InstitutionSearch>() { InstitutionSearch.ID, InstitutionSearch.NAME, InstitutionSearch.ADRESS };
        }
        private bool CustomFilter(object obj)
        {
            Institution ins = obj as Institution;
            if (selectedFilter.Equals("SEARCH_ID"))
            {
                if (!txtSearch.Text.Equals(""))
                {
                    return ins.Active && ins.InsId == (int.Parse(txtSearch.Text));
                }
                else
                    return true;

            }
            else if (selectedFilter.Equals("SEARCH_NAME"))
            {
                return ins.Active && ins.InsName.Contains(txtSearch.Text);
            }
            else
            {
                return ins.Active && ins.InsAdress.Contains(txtSearch.Text);
            }
        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Institutions);
            dgInstitutions.ItemsSource = view;
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (cmbSearchIns.SelectedItem.Equals(InstitutionSearch.ID))
            {
                selectedFilter = "SEARCH_ID";
            }
            else if (cmbSearchIns.SelectedItem.Equals(InstitutionSearch.NAME))
            {
                selectedFilter = "SEARCH_NAME";
            }
            else
            {
                selectedFilter = "SEARCH_ADRESS";
            }
            view.Refresh();
        }
        private void dgInstitutions_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Classrooms")
                || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow l = new LoginWindow();
            l.Show();
            this.Close();
        }

        private void btnSchedule_Click(object sender, RoutedEventArgs e)
        {
            GuestAppointments g = new GuestAppointments();
            g.Show();
            this.Close();
        }

        private void btnEmployees_Click(object sender, RoutedEventArgs e)
        {    
            GuestEmployees c = new GuestEmployees();
            c.Show();
            this.Close();   
        }
    }
}
