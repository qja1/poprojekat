﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for ClassroomsWindow.xaml
    /// </summary>
    public partial class ClassroomsWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "Active";
        int ins;
        public ClassroomsWindow(int InsId)
        {
            this.ins = InsId;
            InitializeComponent();
            InitializeView();
            view.Filter = CustomFilter;
            cmbSearchClass.ItemsSource = new List<ClassroomSearch>() { ClassroomSearch.ID, ClassroomSearch.NUMBER , ClassroomSearch.SEATS};
        }
        private bool CustomFilter(object obj)
        {
            Classroom c = obj as Classroom;
            if (selectedFilter.Equals("SEARCH_ID"))
            {
                if (!txtSearch.Text.Equals(""))
                {
                    return c.Active && c.ClassId == (int.Parse(txtSearch.Text));
                }
                else
                    return true;
            }
            else if (selectedFilter.Equals("SEARCH_NUMBER"))
            {
                if (!txtSearch.Text.Equals(""))
                {
                    return c.Active && c.ClassNumber == (int.Parse(txtSearch.Text));
                }
                return true;
            }
            else
            {
                if (!txtSearch.Text.Equals(""))
                {
                    return c.Active && c.SeatsNumber == (int.Parse(txtSearch.Text));
                }
                else
                    return true;             
            }
          
        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Classrooms.FindAll(c => c.InsId == ins));
            dgClassrooms.ItemsSource = view;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            
            Classroom c = new Classroom();
            c.InsId = ins;
            ClassroomForm cForm = new ClassroomForm(c);
            if (cForm.ShowDialog() == true)
            {
                selectedFilter = "Active";
                view.Refresh();     
            }            
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Classroom classroom = dgClassrooms.SelectedValue as Classroom;
            if (classroom == null)
            {
                MessageBox.Show("Classroom not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Classroom oldClassroom = classroom.Clone();
                ClassroomForm classroomForm = new ClassroomForm(classroom);
                if (classroomForm.ShowDialog() == false)
                {
                    int index = Data.Classrooms.FindIndex(u => u.ClassId == oldClassroom.ClassId);
                    Data.Classrooms[index] = oldClassroom;
                }
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Classroom classroom = dgClassrooms.SelectedValue as Classroom;
            if (classroom != null)
            {
                Classroom classroomDelete = Data.GetClassroomById(classroom.ClassId);
                classroomDelete.Active = false;
                classroomDelete.DeleteClassroom();
                selectedFilter = "Active";
                view.Refresh();

            }
            else
            {
                MessageBox.Show("Classroom does not exist", "Warning", MessageBoxButton.OK);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            

            if (cmbSearchClass.SelectedItem.Equals(ClassroomSearch.ID))
            {
                selectedFilter = "SEARCH_ID";
            }
            else if (cmbSearchClass.SelectedItem.Equals(ClassroomSearch.NUMBER))
            {
                selectedFilter = "SEARCH_NUMBER";
            }
            else
            {
                selectedFilter = "SEARCH_SEATS";
            }
            view.Refresh();
        }

        private void dgClassrooms_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error")
                || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            InstitutionsWindow i = new InstitutionsWindow();
            i.Show();
            this.Close();
        }


    }
}