﻿using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POProjekat.Models
{
    public class Professor : User
    {
        private ObservableCollection<User> _assistants;


        public ObservableCollection<User> Assistants
        {
            get { return _assistants; }
            set { _assistants = value; }
        }

        private int _insId;
        private object insId;

        public int InsId
        {
            get { return _insId; }
            set { _insId = value; }
        }

        public Professor(string name, string surname, string username, string email, bool active, string password, int insId, ObservableCollection<User> assistants ) : base(name, surname, username, email, active, password)
        {
            TypeOfUsers = TypeOfUser.PROFESSOR;
            this._assistants = assistants;
        }

        public Professor(string name, string surname, string username, string email, bool active, string password) : base(name, surname, username, email, active, password)
        {
        }

        public Professor(string name, string surname, string username, string email, bool active, string password, object insId) : this(name, surname, username, email, active, password)
        {
            this.insId = insId;
        }

        public override User Clone()
        {
            Professor professor = new Professor(Name, Surname, Username, Email, Active, Password, InsId, Assistants);
            professor.Assistants = Assistants;
            return professor;
        }

        public override int SaveUser()
        {
            int id = base.SaveUser();
 
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Professors (id) values (@id)";

                command.Parameters.Add(new SqlParameter("id", id));

                command.ExecuteNonQuery();
            }

            return id;

        }

        public override void DeleteUser()
        {

            User user = Data.GetUserByUsername(this.Username);

            base.DeleteUser();

            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
         
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update professors set active=@Active where id = @id";
                command.Parameters.Add(new SqlParameter("id", user.Id));
                command.Parameters.Add(new SqlParameter("Active", false));

                command.ExecuteNonQuery();
            }
        }

    }
}

