﻿using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POProjekat.Models
{
    public class Administrator : User
    {
        public Administrator()
        {
        }

        public Administrator(string name, string surname, string username, string email, bool active, string password) : base(name, surname, username, email, active, password)
        {
            TypeOfUsers = TypeOfUser.ADMIN;

        }

        public override User Clone()
        {
            Administrator administrator = new Administrator(Name, Surname, Username, Email, Active, Password);
            return administrator;
        }

        public override int SaveUser()
        {
            int id = base.SaveUser();

            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into Administrators (id) values (@id)";

                command.Parameters.Add(new SqlParameter("id", id));

                command.ExecuteNonQuery();
            }

            return id;

        }

        public override void DeleteUser()
        {

            User user = Data.GetUserByUsername(this.Username);

            base.DeleteUser();

            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update administrators set active=@Active where id = @id";
                command.Parameters.Add(new SqlParameter("id", user.Id));
                command.Parameters.Add(new SqlParameter("Active", false));

                command.ExecuteNonQuery();
            }
        }

    }
}


