﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POProjekat.Models
{
     public enum TypeOfUser
     {
         ADMIN,
         PROFESSOR,
         TA
     }

    public enum UserSortSearch
    {
        USERNAME,
        NAME,
        SURNAME,
        EMAIL
    }

    public enum InstitutionSearch
    {
        ID,
        NAME,
        ADRESS
    }

    public enum ClassroomSearch
    {
        ID,
        NUMBER,
        SEATS
    }

    public enum TeachingForm
    {
        LECTURE,
        PRACTISE,
        UNSPECIFIED
    }
}
