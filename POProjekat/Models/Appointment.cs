﻿using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POProjekat.Models
{
    public class Appointment : INotifyPropertyChanged
    {
        private int id;
        private bool active;
        private DateTime startDate;
        private DateTime endDate;
        private int classroomId; 
        private TeachingForm teachingForm;
        private int userId;
        

        public Appointment(int id, bool active, DateTime start, DateTime end, int classroomId, TeachingForm teachingForm, int userId)
        {
            this.id = id;
            this.active = active;
            this.startDate = start;
            this.endDate = end;
            this.classroomId = classroomId;
            this.teachingForm = teachingForm;
            this.userId = userId;
        }

        public Appointment() : this(-1, true, DateTime.Now, DateTime.Now, 0, TeachingForm.UNSPECIFIED, 0) { }

        public Appointment(bool active, DateTime startDate, DateTime endDate, int classroomId, TeachingForm teachingForm, int userId)
        {
            this.active = active;
            this.startDate = startDate;
            this.endDate = endDate;
            this.classroomId = classroomId;
            this.teachingForm = teachingForm;
            this.userId = userId;
        }

        public int Id 
        {
            get { return id; }
            set { id = value; }
        }

        public DateTime StartDate { get { return startDate; } set { startDate = value; OnPropertyChanged("StartDate"); } }

        public DateTime EndDate { get { return endDate; } set { endDate = value; OnPropertyChanged("EndDate"); } }
        public bool Active { get { return active; } set { active = value; OnPropertyChanged("Active"); } }

        public int UserId { get { return userId; } set { userId = value; OnPropertyChanged("UserId"); } }
        public int ClassroomId { get { return classroomId; } set { classroomId = value; OnPropertyChanged("ClassroomId"); } }

        public TeachingForm TeachingForm
        {
            get { return teachingForm; }
            set { teachingForm = value; OnPropertyChanged("TeachingForm"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public virtual Appointment Clone()
        {
            Appointment app = new Appointment(Id, Active, StartDate, EndDate, ClassroomId, TeachingForm, UserId);
            return app;         
        }

        public virtual int SaveAppointment()
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                Console.WriteLine("USAO U ADD");
                command.CommandText = @"insert into Appointment (active, startDate, endDate, classroomId, teachingForm, userId) output inserted.id values (@Active, @StartDate, @EndDate, @ClassroomId, @TeachingForm, @UserId)";
                command.Parameters.Add(new SqlParameter("Active", this.Active));
                command.Parameters.Add(new SqlParameter("StartDate", this.StartDate));
                command.Parameters.Add(new SqlParameter("EndDate", this.EndDate));
                command.Parameters.Add(new SqlParameter("ClassroomId", this.ClassroomId));

                int tf;
                switch (this.teachingForm)
                {
                    case TeachingForm.LECTURE: tf = 0; break;
                    case TeachingForm.PRACTISE: tf = 1; break;
                    default: tf = -1;  break;
                }

                command.Parameters.Add(new SqlParameter("TeachingForm", tf));                
                command.Parameters.Add(new SqlParameter("UserId", this.UserId));

                id = (int)command.ExecuteScalar();

            }


            return id;
        }

        public virtual void DeleteAppointment()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update appointment set active=@Active where id = @Id";
                command.Parameters.Add(new SqlParameter("Id", this.Id));
                command.Parameters.Add(new SqlParameter("Active", false));


                command.ExecuteNonQuery();
            }
        }

        public virtual void UpdateAppointment()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                Console.WriteLine("USAO U EDIT");
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update appointment set startDate=@StartDate, endDate=@EndDate, classroomId=@ClassroomId, teachingForm=@TeachingForm, userId=@UserId where id=@Id";               
                command.Parameters.Add(new SqlParameter("StartDate", this.StartDate));
                command.Parameters.Add(new SqlParameter("EndDate", this.EndDate));
                command.Parameters.Add(new SqlParameter("ClassroomId", this.ClassroomId));
                command.Parameters.Add(new SqlParameter("TeachingForm", this.teachingForm.ToString()));
                command.Parameters.Add(new SqlParameter("UserId", this.UserId));
                command.Parameters.Add(new SqlParameter("Id", this.Id));

                command.ExecuteNonQuery();
            }
        }
    }
}
