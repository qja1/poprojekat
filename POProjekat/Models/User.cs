﻿using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POProjekat.Models
{
    public class User: INotifyPropertyChanged, IDataErrorInfo
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value; OnPropertyChanged("Name");
            }


        }

        private string _surname;

        public string Surname
        {
            get { return _surname; }
            set { _surname = value; OnPropertyChanged("Surname"); }
        }
        

        private string _username;

        public string Username
        {
            get { return _username; }
            set { _username = value; OnPropertyChanged("Username"); }
        }



        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        private TypeOfUser _typeOfUser;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        

        public TypeOfUser TypeOfUsers
        {
            get { return _typeOfUser; }
            set { _typeOfUser = value; OnPropertyChanged("TypeOfUser"); }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _password;

        public string Password
        {
            get { return  _password; }
            set {  _password = value; }
        }


        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Name":
                        if (Name != null && Name.Equals(string.Empty))
                            return "This fieldis required!";
                        break;
                }
                return string.Empty;
            }
        }

        public User(string name, string surname, string username, string email, bool active, string password)
        {
            this._name = name;
            this._surname = surname;
            this._username = username;
            this._email = email;
            this._active = true; 
            this._password = password;

        }

        public virtual User Clone()
        {
            return null;
        }

        public static Boolean UserExist(String username)
        {
            foreach (var user in Data.Users)
            {
                if (user.Username == username)
                {
                    return true;
                }
            }
            return false;
        }

        public static User UserExist(String username, String password)
        {
            foreach (var user in Data.Users)
            {
                if (user.Username == username && user.Password == password)
                {
                    return user;
                }
            }
            return null;
        }


        public User()
        {
            Username = "";
            Active = true;
        }

        public virtual int SaveUser()
        {
            int id = -1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"insert into Users (name, surname, username, typeofuser, email, active, password) output inserted.id values (@Name, @Surname, @Username, @TypeOfUser, @Email, @Active, @Password)";
                command.Parameters.Add(new SqlParameter("Name", this.Name));
                command.Parameters.Add(new SqlParameter("Surname", this.Surname));
                command.Parameters.Add(new SqlParameter("Username", this.Username));
                command.Parameters.Add(new SqlParameter("TypeOfUser", this.TypeOfUsers.ToString()));
                command.Parameters.Add(new SqlParameter("Email", this.Email));
                command.Parameters.Add(new SqlParameter("Active", this.Active));
                command.Parameters.Add(new SqlParameter("Password", this.Password));

                id = (int)command.ExecuteScalar();

            }


            return id;
        }

        public virtual void DeleteUser()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update users set active=@Active where username = @Username";
                command.Parameters.Add(new SqlParameter("Username", this.Username));
                command.Parameters.Add(new SqlParameter("Active", false));


                command.ExecuteNonQuery();
            }
        }

        public virtual void UpdateUser()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"update users set name=@Name, surname=@Surname, email=@Email, password=@Password where username=@Username";
                command.Parameters.Add(new SqlParameter("Name", this.Name));
                command.Parameters.Add(new SqlParameter("Surname", this.Surname));
                command.Parameters.Add(new SqlParameter("Email", this.Email));
                command.Parameters.Add(new SqlParameter("Password", this.Password));
                command.Parameters.Add(new SqlParameter("Username", this.Username));

                command.ExecuteNonQuery();
            }
        }
    }
}

