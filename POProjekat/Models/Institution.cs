﻿using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POProjekat.Models
{
    public class Institution : INotifyPropertyChanged, IDataErrorInfo
    {
		private int _insId;

		public int InsId
		{
			get { return _insId; }
			set { _insId = value; OnPropertyChanged("InsId"); }
		}

		private string _insName;

		public string InsName
		{
			get { return _insName; }
			set { _insName = value; OnPropertyChanged("InsName"); }
		}

		private string _insAdress;

		public string InsAdress
		{
			get { return _insAdress; }
			set { _insAdress = value; OnPropertyChanged("InsAdress"); }
		}
		private bool _active;

		public bool Active
		{
			get { return _active; }
			set { _active = value; OnPropertyChanged("Active"); }
		}

		private ObservableCollection<Classroom> _classrooms;

		public ObservableCollection<Classroom> Classrooms
		{
			get { return _classrooms; }
			set { _classrooms = value; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}


		public string Error
		{
			get { return null; }
		}

		public string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "InsName":
						if (InsName != null && InsName.Equals(string.Empty))
							return "This field is required!";
						break;
				}
				return string.Empty;
			}
		}
		
		
		public Institution(int insId, string insName, string insAdress, bool active, ObservableCollection<Classroom> classrooms )
		{
			this._insId = insId;
			this._insName = insName;
			this._insAdress = insAdress;
			this._active = active;
			this._classrooms = classrooms;
		}
		public Institution(string insName, string insAdress, bool active, ObservableCollection<Classroom> classrooms)
		{
			this._insName = insName;
			this._insAdress = insAdress;
			this._active = active;
			this._classrooms = classrooms;
		}

		public Institution Clone()
		{
			Institution institution = new Institution(InsId, InsName, InsAdress, Active, Classrooms);
			institution.Classrooms = Classrooms;
			return institution;
		}

		public Institution()
		{
			InsName = "";
			Active = true;
		}

		public virtual int SaveInstitution()
		{
			int id = -1;

			using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
			{
				conn.Open();
				SqlCommand command = conn.CreateCommand();

				command.CommandText = @"insert into institutions (insName, insAdress, active) output inserted.insId values (@InsName, @InsAdress, @Active)";
				command.Parameters.Add(new SqlParameter("InsName", this.InsName));
				command.Parameters.Add(new SqlParameter("InsAdress", this.InsAdress));
				command.Parameters.Add(new SqlParameter("Active", this.Active));

				id = (int)command.ExecuteScalar();
			}
			return id;
		}

		public virtual void DeleteInstitution()
		{
			using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
			{
				conn.Open();
				SqlCommand command = conn.CreateCommand();

				command.CommandText = @"update institutions set active=@Active where insId = @InsId";
				command.Parameters.Add(new SqlParameter("InsId", this.InsId));
				command.Parameters.Add(new SqlParameter("Active", false));


				command.ExecuteNonQuery();
			}
		}

		public virtual void UpdateInstitution()
		{
			using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
			{
				conn.Open();
				SqlCommand command = conn.CreateCommand();
				
				command.CommandText = @"update institutions set insName=@InsName, insAdress=@InsAdress where insId=@InsId";
				command.Parameters.Add(new SqlParameter("InsName", this.InsName));
				command.Parameters.Add(new SqlParameter("InsAdress", this.InsAdress));
				command.Parameters.Add(new SqlParameter("InsId", this.InsId));

				command.ExecuteNonQuery();
			}
		}
	}
}
