﻿using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POProjekat.Models
{
    public class TeacherAssistant : User
    {
        private int _professor;

        public int Professor
        {
            get { return _professor; }
            set { _professor = value; }
        }

        private int _insId;

        public int InsId
        {
            get { return _insId; }
            set { _insId = value; }
        }

        public TeacherAssistant(string name, string surname, string username, string email, bool active, string password, int professor, int insId) : base(name, surname, username, email, active, password)
        {
            this._professor = professor;
            TypeOfUsers = TypeOfUser.TA;
        }

        public TeacherAssistant()
        {
        }

        public TeacherAssistant(string name, string surname, string username, string email, bool active, string password) : base(name, surname, username, email, active, password)
        {
        }

        public override User Clone()
        {
            TeacherAssistant teacherAssistant = new TeacherAssistant(Name, Surname, Username, Email, Active, Password, Professor, InsId);
            return teacherAssistant;
        }

        public override int SaveUser()
        {
            int id = base.SaveUser();

            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into teacherAssistants (id) values (@id)";

                command.Parameters.Add(new SqlParameter("id", id));

                command.ExecuteNonQuery();
            }

            return id;

        }

        public override void DeleteUser()
        {

            User user = Data.GetUserByUsername(this.Username);

            base.DeleteUser();

            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update teacherAssistants set active=@Active where id = @id";
                command.Parameters.Add(new SqlParameter("id", user.Id));
                command.Parameters.Add(new SqlParameter("Active", false));

                command.ExecuteNonQuery();
            }
        }

    }
}
