﻿using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POProjekat.Models
{
    public class Classroom : INotifyPropertyChanged, IDataErrorInfo
    {
		private int _classId;

		public int ClassId
		{
			get { return _classId; }
			set { _classId = value; }
		}

		private int _classNumber;

		public int ClassNumber
		{
			get { return _classNumber; }
			set { _classNumber = value; OnPropertyChanged("ClassNumber"); }
		}

		private int _seatsNumber;

		public int SeatsNumber
		{
			get { return _seatsNumber; }
			set { _seatsNumber = value; OnPropertyChanged("SeatsNumber"); }
		}

		private bool _active;

		public bool Active
		{
			get { return _active; }
			set { _active = value; OnPropertyChanged("Active"); }
		}

		private int _insId;

		public int InsId
		{
			get { return _insId; }
			set { _insId = value; OnPropertyChanged("InsId"); }
		}
		private int _institutionId;

		

		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		public string Error
		{
			get { return null; }
		}

		public string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "ClassNumber":
						if (ClassNumber != -1 && ClassNumber.Equals(string.Empty))
							return "This fieldis required!";
						break;
				}
				return string.Empty;
			}
		}

		public Classroom(int classId, int classNumber, int seatsNumber, bool active,int insId )
		{
			this._classId = classId;
			this._classNumber = classNumber;
			this._seatsNumber = seatsNumber;
			this._active = active;
			this._insId = insId;
		}

        public Classroom()
        {
        }

		public Classroom(int classNumber, int seatsNumber, bool active)
		{
			ClassNumber = classNumber;
			SeatsNumber = seatsNumber;
			Active = active;
		}

		public virtual Classroom Clone()
		{
			return null;
		}

		public virtual int SaveClassroom()
		{
			
			int id = -1;
			using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
			{
				conn.Open();
				SqlCommand command = conn.CreateCommand();

				command.CommandText = @"insert into Classrooms (ClassNumber, SeatsNumber, Active, InsId) output inserted.ClassId values (@ClassNumber, @SeatsNumber, @Active, @InsId)";
				command.Parameters.Add(new SqlParameter("ClassNumber", this.ClassNumber));
				command.Parameters.Add(new SqlParameter("SeatsNumber", this.SeatsNumber));
				command.Parameters.Add(new SqlParameter("Active", this.Active));
				command.Parameters.Add(new SqlParameter("InsId", this.InsId));

				id = (int)command.ExecuteScalar();
				Console.WriteLine("DODAO UCIONICU");
			}


			return id;
		}

		public virtual void DeleteClassroom()
		{
			using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
			{
				conn.Open();
				SqlCommand command = conn.CreateCommand();

				command.CommandText = @"update clasrooms set active=@Active where classId = @ClassId";
				command.Parameters.Add(new SqlParameter("ClassId", this.ClassId));
				command.Parameters.Add(new SqlParameter("Active", false));


				command.ExecuteNonQuery();
			}
		}

		public virtual void UpdateClassroom()
		{
			using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
			{
				conn.Open();
				SqlCommand command = conn.CreateCommand();

				command.CommandText = @"update classrooms set classNumber=@ClassNumber, seatsNumber=@SeatsNumber, insId=@InsId where classId=@ClassId";
				command.Parameters.Add(new SqlParameter("ClassNumber", this.ClassNumber));
				command.Parameters.Add(new SqlParameter("SeatsNumber", this.SeatsNumber));
				command.Parameters.Add(new SqlParameter("InsId", this.InsId));
				command.Parameters.Add(new SqlParameter("ClassId", this.ClassId));


				command.ExecuteNonQuery();
			}
		}

	}
}
