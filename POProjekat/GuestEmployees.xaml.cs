﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for GuestEmployees.xaml
    /// </summary>
    public partial class GuestEmployees : Window
    {
        ICollectionView view;
        String selectedFilter = "Active";
        public GuestEmployees()
        {
            InitializeComponent();
            InitializeView();
        }
        private void InitializeView()
        {
            List<User> listProfAndAss = new List<User>();
            foreach (User user in Data.Users)
            {
                if (user.TypeOfUsers == TypeOfUser.PROFESSOR || user.TypeOfUsers == TypeOfUser.TA)
                    {               
                        listProfAndAss.Add(user);
                    }
            }
            view = CollectionViewSource.GetDefaultView(listProfAndAss);
            dgEmployees.ItemsSource = view;
        }
        
        private void dgEmployees_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error")
                || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            GuestWindow g = new GuestWindow();
            g.Show();
            this.Close();
        }
    }
}
