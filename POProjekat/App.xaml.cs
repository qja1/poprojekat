﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;


namespace POProjekat
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Data.ReadActiveUsers(); 
            Data.ReadClassrooms();
            Data.ReadInstitutions();
            Data.ReadAppointments();

            LoginWindow window = new LoginWindow();
            window.Show();
        }
    }
}
