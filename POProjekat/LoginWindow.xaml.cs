﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            string username = txtUsername.Text;
            string password = pbPassword.Password;

            User LoggedUser = User.UserExist(username, password);

            if (LoggedUser == null)
            {
                MessageBox.Show("User data is not correct.", "");
            }
            else
            {
                Data.LoggedUser = LoggedUser;
                if (LoggedUser.TypeOfUsers == TypeOfUser.ADMIN)
                {
                    AdminWindow aw = new AdminWindow();
                    aw.Show();
                    this.Close();
                }
                if (LoggedUser.TypeOfUsers == TypeOfUser.PROFESSOR)
                {
                    ProfessorWindow pw = new ProfessorWindow();
                    pw.Show();
                    this.Close();
                }
                if (LoggedUser.TypeOfUsers == TypeOfUser.TA)
                {
                    TAWindow tw = new TAWindow();
                    tw.Show();
                    this.Close();
                }
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnGuest_Click(object sender, RoutedEventArgs e)
        {
            GuestWindow gw = new GuestWindow();
            gw.Show();
            this.Close();
        }
    }
}
