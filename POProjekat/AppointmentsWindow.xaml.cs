﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class AppointmentsWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "Active";

        public AppointmentsWindow()
        {
            InitializeComponent();
            InitializeView();
        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Appointments);
            dgAppointments.ItemsSource = view;
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Appointment a = new Appointment();
            AppointmentForm aForm = new AppointmentForm(a);
            if (aForm.ShowDialog() == true)
            {
                selectedFilter = "Active";
                view.Refresh();
            }
        }
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Appointment a = dgAppointments.SelectedItem as Appointment;

            if (a == null)
            {
                MessageBox.Show("Appointment not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Appointment oldApp = a.Clone();
                AppointmentForm appForm = new AppointmentForm(a);
                
                if (appForm.ShowDialog() == false)
                {
                    int index = Data.Appointments.FindIndex(u => u.Id == oldApp.Id);
                    Data.Appointments[index] = oldApp;
                }
                selectedFilter = "Active";
                view.Refresh();
            }
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            Appointment app = dgAppointments.SelectedValue as Appointment;
            if (app != null)
            {
                Appointment appDelete = Data.GetAppointmentById(app.Id);
                appDelete.Active = false;
                appDelete.DeleteAppointment();
                selectedFilter = "Active";
                view.Refresh();
            }
            else
            {
                MessageBox.Show("Appointment not selected", "Warning", MessageBoxButton.OK);
            }
        }
        private void dgAppointments_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error")
                || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            InstitutionsWindow i = new InstitutionsWindow();
            i.Show();
            this.Close();

        }
    }
}
