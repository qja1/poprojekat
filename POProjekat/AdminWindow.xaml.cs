﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {

        ICollectionView view;
        String selectedFilter = "Active";
       
        public AdminWindow()
        {
            InitializeComponent();
            InitializeView();
            view.Filter = CustomFilter;
            cmbSearch.ItemsSource = new List<UserSortSearch>() { UserSortSearch.NAME, UserSortSearch.SURNAME, UserSortSearch.USERNAME, UserSortSearch.EMAIL };
        }

        private bool CustomFilter(object obj)
        {
            User user = obj as User;
            if (selectedFilter.Equals("SEARCH_USERNAME"))
            {
                return user.Active && user.Username.Contains(txtSearchUsername.Text);
            }
            else if (selectedFilter.Equals("SEARCH_NAME"))
            {
                return user.Active && user.Name.Contains(txtSearchUsername.Text);
            }
            else if (selectedFilter.Equals("SEARCH_SURNAME"))
            {
                return user.Active && user.Surname.Contains(txtSearchUsername.Text);
            }
            else 
            {
                return user.Active && user.Email.Contains(txtSearchUsername.Text);
            }            
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Users);
            dgUsers.ItemsSource = view;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            User user = new User();
            UserWindow userWindow = new UserWindow(user);
            if (userWindow.ShowDialog() == true)
            {
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            User user = dgUsers.SelectedValue as User;
            if (user != null)
            {
                User userDelete = Data.GetUserByUsername(user.Username);
                userDelete.Active = false;
                userDelete.DeleteUser();
                selectedFilter = "Active";
                view.Refresh();
            }
            else
            {
                MessageBox.Show("User does not exist", "Warning", MessageBoxButton.OK);
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            User user = dgUsers.SelectedItem as User;

            if (user == null)
            {
                MessageBox.Show("User not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                User oldUser = user.Clone();
                UserWindow userWindow = new UserWindow(user);
                if (userWindow.ShowDialog() == false)
                {
                    int index = Data.Users.FindIndex(u => u.Username.Equals(oldUser.Username));
                    Data.Users[index] = oldUser;
                }
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (cmbSearch.SelectedItem.Equals(UserSortSearch.NAME))
            {
                selectedFilter = "SEARCH_NAME";
            }
            else if (cmbSearch.SelectedItem.Equals(UserSortSearch.SURNAME))
            {
                selectedFilter = "SEARCH_SURNAME";
            }
            else if (cmbSearch.SelectedItem.Equals(UserSortSearch.USERNAME))
            {
                selectedFilter = "SEARCH_USERNAME";
            }
            else 
            {
                selectedFilter = "SEARCH_EMAIL";
            }           
            view.Refresh();
        }
        
        private void dgUsers_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error")
                || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void btnInstitutions_Click(object sender, RoutedEventArgs e)
        { 
            InstitutionsWindow institutionsWindow = new InstitutionsWindow();
            institutionsWindow.Show();
            this.Close();  
        }

        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow lw = new LoginWindow();
            lw.Show();
            this.Close();
        }
    }
}

        

