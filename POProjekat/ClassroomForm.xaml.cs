﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for ClassroomForm.xaml
    /// </summary>
    public partial class ClassroomForm : Window
    {
        enum Status { ADD, EDIT };
        private Status _status;
        private Classroom selectedClassroom;

        public ClassroomForm(Classroom classroom)
        {
            InitializeComponent();
            if (classroom.ClassNumber != 0)
            {
                this._status = Status.EDIT;
            }
            else
            {
                this._status = Status.ADD;
            }
            selectedClassroom = classroom;
            this.DataContext = classroom;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Classroom c = Data.GetClassroomByNumber(int.Parse(txtNumber.Text));
            if (c != null && _status.Equals(Status.ADD))
            {
                MessageBox.Show($"Classroom with number {c.ClassNumber} exists!", "Warning", MessageBoxButton.OK);
                return;
            }
            Classroom c2;

            if (_status.Equals(Status.ADD))
            {
                c2 = new Classroom(selectedClassroom.ClassNumber, selectedClassroom.SeatsNumber, true);
                c2.InsId = selectedClassroom.InsId;
            }
            else
            {
                c2 = new Classroom(selectedClassroom.ClassNumber, selectedClassroom.SeatsNumber, selectedClassroom.Active);
            }


            if (_status.Equals(Status.ADD))
            {             
                int idClass = c2.SaveClassroom();
                c2.ClassId = idClass;
                Data.Classrooms.Add(c2);
            }
            if (_status.Equals(Status.EDIT))
            {
                c2.UpdateClassroom();
            }
            this.DialogResult = true;
            this.Close();
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
