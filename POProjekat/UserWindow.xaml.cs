﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class UserWindow : Window
    {
        enum Status { ADD, EDIT }
        private Status _status;
        private User selectedUser;


        public UserWindow(User user)
        {
            InitializeComponent();
            cmbType.ItemsSource = new List<TypeOfUser>() { TypeOfUser.ADMIN, TypeOfUser.PROFESSOR, TypeOfUser.TA };
            
            if (user.Username.Equals(""))
            {
                this._status = Status.ADD;
            }
            else
            {
                this._status = Status.EDIT;

                txtUsername.IsReadOnly = true;
                cmbType.IsEnabled = false;
            }
            selectedUser = user;
            this.DataContext = user;     
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            User user = Data.GetUserByUsername(txtUsername.Text);

            if (user != null && _status.Equals(Status.ADD))
            {
                MessageBox.Show($"User with username {user.Username} exists!", "Warning", MessageBoxButton.OK);
                return;
            }

            User us;

            if (selectedUser.TypeOfUsers.Equals(TypeOfUser.ADMIN))
            {
                us = new Administrator(selectedUser.Name, selectedUser.Surname, selectedUser.Username, selectedUser.Email, selectedUser.Active, selectedUser.Password);
            }
            else if (selectedUser.TypeOfUsers.Equals(TypeOfUser.PROFESSOR))
            {
                us = new Professor(selectedUser.Name, selectedUser.Surname, selectedUser.Username, selectedUser.Email, selectedUser.Active, selectedUser.Password);
            }
            else
            {
                us = new TeacherAssistant(selectedUser.Name, selectedUser.Surname, selectedUser.Username, selectedUser.Email, selectedUser.Active, selectedUser.Password);
            }

            if (_status.Equals(Status.ADD))
            {
                int idUser = us.SaveUser();
                us.Id = idUser;
                Data.Users.Add(us);
            }
            if (_status.Equals(Status.EDIT))

                us.UpdateUser();

            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        
    }
}
