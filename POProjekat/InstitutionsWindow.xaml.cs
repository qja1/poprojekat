﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for InstitutionsWindow.xaml
    /// </summary>
    public partial class InstitutionsWindow : Window
    {
        Institution institution;
        ICollectionView view;
        String selectedFilter = "Active";

        public InstitutionsWindow()
        {
            InitializeComponent();
            InitializeView();
            view.Filter = CustomFilter;
            cmbSearchIns.ItemsSource = new List<InstitutionSearch>() { InstitutionSearch.ID, InstitutionSearch.NAME, InstitutionSearch.ADRESS };
        }
        private bool CustomFilter(object obj)
        {
            Institution ins = obj as Institution;
            if (selectedFilter.Equals("SEARCH_ID"))
            {
                if (!txtSearch.Text.Equals(""))
                {
                    return ins.Active && ins.InsId == (int.Parse(txtSearch.Text));
                }
                else 
                    return true;           
            }
            else if (selectedFilter.Equals("SEARCH_NAME"))
            {
                return ins.Active && ins.InsName.Contains(txtSearch.Text);
            }
            else
            {
                return ins.Active && ins.InsAdress.Contains(txtSearch.Text);
            }
        }

        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Institutions);
            dgInstitutions.ItemsSource = view;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Institution i = new Institution();
            InstitutionForm iForm = new InstitutionForm(i);
            if (iForm.ShowDialog() == true)
            {
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Institution institution = dgInstitutions.SelectedValue as Institution;
            if (institution == null)
            {
                MessageBox.Show("Institution not selected", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Institution oldInstitution = institution.Clone();
                InstitutionForm institutionForm = new InstitutionForm(institution);
                
                if (institutionForm.ShowDialog() == false)
                {
                    int index = Data.Institutions.FindIndex(u => u.InsId == oldInstitution.InsId);
                    Data.Institutions[index] = oldInstitution;
                }
                selectedFilter = "Active";
                view.Refresh();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            institution = dgInstitutions.SelectedValue as Institution;
            if (institution != null)
            {
                Institution institutionDelete = Data.GetInstitutionById(institution.InsId);
                institutionDelete.Active = false;
                institutionDelete.DeleteInstitution();
                selectedFilter = "Active";
                view.Refresh();
            }
            else
            {
                MessageBox.Show("Institution does not exist", "Warning", MessageBoxButton.OK);
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (cmbSearchIns.SelectedItem.Equals(InstitutionSearch.ID))
            {
                selectedFilter = "SEARCH_ID";
            }
            else if (cmbSearchIns.SelectedItem.Equals(InstitutionSearch.NAME))
            {
                selectedFilter = "SEARCH_NAME";
            }
            else 
            {
                selectedFilter = "SEARCH_ADRESS";
            }
            view.Refresh();
        }

        private void dgInstitutions_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Classrooms")
                || e.PropertyName.Equals("Active"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow a = new AdminWindow();
            a.Show();
            this.Close();
        }

        private void btnClassrooms_Click(object sender, RoutedEventArgs e)
        {
            institution = dgInstitutions.SelectedValue as Institution;
            if (institution != null)
            {
                ClassroomsWindow c = new ClassroomsWindow(institution.InsId);
                c.Show();
                this.Close();
            } else
            {
                MessageBox.Show("Institution not selected", "Warning", MessageBoxButton.OK);
            }
            
        }

        private void btnAppoitments_Click(object sender, RoutedEventArgs e)
        {
            AppointmentsWindow a = new AppointmentsWindow();
            a.Show();
            this.Close();
        }
    }
}
