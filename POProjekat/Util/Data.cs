﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POProjekat.Models;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;


namespace POProjekat.Util
{
    class Data
    {
        public static string CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static List<TeacherAssistant> Assistants { get; set; }
        public static List<Professor> Professors { get; set; }
        public static List<Administrator> Administrators { get; set; }
        public static List<User> Users { get; set; }
        public static List<Institution> Institutions { get; set; }
        public static List<Classroom> Classrooms { get; set; }
        public static List<Appointment> Appointments { get; set; }




        public static void ReadActiveUsers()
        {
            Users = new List<User>();

            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from users where active = @active";
                command.Parameters.Add(new SqlParameter("active", true));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Users");

                //asistenti
                List<TeacherAssistant> ass = new List<TeacherAssistant>();
                foreach (DataRow row in ds.Tables["Users"].Rows)
                {     
                    int id = (int)row["Id"];
  
                    if (row["TypeOfUser"].Equals(TypeOfUser.TA.ToString())  )
                    {  
                        TeacherAssistant newAss = findAss(id);

                        if (newAss != null) ass.Add(newAss);
                    }
                }
                Assistants = ass;

                //prof
                List<Professor> professors = new List<Professor>();
                foreach (DataRow row in ds.Tables["Users"].Rows)
                {

                    int id = (int)row["Id"];

                    if (row["TypeOfUser"].Equals(TypeOfUser.PROFESSOR.ToString()))
                    {

                       Professor prof = findProf(id);
                        if (prof != null) professors.Add(prof);

                    }

                }
                Professors = professors;

                //admins
                List<Administrator> admins = new List<Administrator>();
                foreach (DataRow row in ds.Tables["Users"].Rows)
                {

                    string name = (string)row["Name"];
                    string surname = (string)row["Surname"];
                    string username = (string)row["Username"];
                    string email = (string)row["Email"];
                    bool active = (bool)row["Active"];
                    string password = (string)row["Password"];
                    int id = (int)row["Id"];

                    if (row["TypeOfUser"].Equals(TypeOfUser.ADMIN.ToString()))
                    {



                        Administrator admin = new Administrator(name, surname, username, email, active, password);
                       
                        admin.Id = id;
                        admins.Add(admin);
                    }
                }
                Administrators = admins;
                addAllusers();         
            }
        }

        private static void addAllusers()
        {
            Assistants.ForEach(a => Users.Add(a));
            Professors.ForEach(p => Users.Add(p));
            Administrators.ForEach(ad => Users.Add(ad));
        }

        private static TeacherAssistant findAss(int idAss)
        {
            List<TeacherAssistant> ass = new List<TeacherAssistant>();
        
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING)) {
                conn.Open();
                var command = conn.CreateCommand();
                var dataAdapter = new SqlDataAdapter();
                command.CommandText = "select r.Id, r.Name, r.Surname, r.Username, r.Email, r.Active, r.Password, a.Professor, a.Institution from Users r, TeacherAssistants a  where r.active = @active and r.Id = " + idAss + " and a.Id = " + idAss;               
                command.Parameters.Add(new SqlParameter("active", true));
                dataAdapter.SelectCommand = command;
                var ds = new DataSet();
                dataAdapter.Fill(ds, "TeacherAssistans");
              
                foreach (DataRow row in ds.Tables["TeacherAssistans"].Rows)
                { 
                    string name = (string)row["Name"];
                    string surname = (string)row["Surname"];
                    string username = (string)row["Username"];
                    string email = (string)row["Email"];
                    bool active = (bool)row["Active"];
                    string password = (string)row["Password"];
                    int id = (int)row["Id"];
                    int insId = (int)row["Institution"];
                    int professorId;
                    if (DBNull.Value.Equals(row["Professor"]))
                    { 
                        professorId = -1;
                    }
                    else
                    {
                        professorId = (int)row["Professor"];
                    }
                    TeacherAssistant ta = new TeacherAssistant(name, surname, username, email, active, password, professorId, insId);
                    ta.Id = id;
                    
                    ass.Add(ta);
                }
                return ass[0];
            }
        }
        private static Professor findProf(int idProf)
        {
            List<Professor> professors = new List<Professor>();

            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select r.Id, r.Name, r.Surname, r.Username, r.Email, r.Active, r.Password, p.Institution from Users r, Professors p  where r.active = @active and r.Id = " + idProf + " and p.Id = " + idProf;
                command.Parameters.Add(new SqlParameter("active", true));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Professors");
                foreach (DataRow row in ds.Tables["Professors"].Rows)
                {
                    string name = (string)row["Name"];
                    string surname = (string)row["Surname"];
                    string username = (string)row["Username"];
                    string email = (string)row["Email"];
                    bool active = (bool)row["Active"];
                    string password = (string)row["Password"];
                    int id = (int)row["Id"];
                    int insId = (int)row["Institution"];

                    Professor p = new Professor(name, surname, username, email, active, password, insId, new ObservableCollection<User>(findAssForProf(id)));
                    p.Id = id;
                    professors.Add(p);
                }
                return professors[0];
            }
        }
        private static List<TeacherAssistant> findAssForProf(int idProf)
        {
            return Assistants.FindAll(a => a.Professor == idProf); 
        }

        public static void SearchByUsername(string username)
            {
            Users.Clear();

            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from users where username like @username";
                command.Parameters.Add(new SqlParameter("username", "%" + username + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds, "Users");

                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    string user_name = (string)row["Username"];
                    string name = (string)row["Name"];
                    string surname = (string)row["Surname"];
                    string email = (string)row["Email"];
                    bool active = (bool)row["Active"];
                    string password = (string)row["Password"];

                    if (row["TypeOfUser"].Equals(TypeOfUser.ADMIN.ToString()))
                    {
                        Administrator a = new Administrator(name, surname, username, email, active, password);
                        Users.Add(a);
                    }
                    //FALI ZA ASISTENTE I PROF
                }

            }
        }
        public static User GetUserByUsername(string username)
        {
            foreach (User user in Users)
            {
                if (user.Username.Equals(username))
                {
                    return user;
                }
            }
            return null;
        }

        public static User LoggedUser = null;

        public static void ReadInstitutions()
        {
            Institutions = new List<Institution>();

            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from institutions where active = @active";
                command.Parameters.Add(new SqlParameter("active", true));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Institutions");
                foreach (DataRow row in ds.Tables["Institutions"].Rows)
                {
                    int insId = (int)row["InsId"];
                    string insName = (string)row["InsName"];
                    string insAdress = (string)row["InsAdress"];
                    bool active = (bool)row["Active"];

                    Institution ins = new Institution(insId, insName, insAdress, active, new ObservableCollection<Classroom>(findClassroomsForInstitution(insId)));
                    Institutions.Add(ins);
                }

            }

        }
        public static List<Classroom> findClassroomsForInstitution(int insId)
        {
            return Classrooms.FindAll(c => c.InsId == insId);
        }

        public static Institution GetInstitutionById(int insId)
        {
            foreach (Institution institution in Institutions)
            {
                if (institution.InsId == insId)
                {
                    return institution;
                }
            }
            return null;
        }
        public static Institution GetInstitutionByName(string insName)
        {
            foreach (Institution institution in Institutions)
            {
                if (institution.InsName.Equals(insName))
                {
                    return institution;
                }
            }
            return null;
        }
        
        public static void ReadClassrooms()
        {
            Classrooms = new List<Classroom>();

            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from classrooms where active = @active";
                command.Parameters.Add(new SqlParameter("active", true));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Classrooms");
                foreach (DataRow row in ds.Tables["Classrooms"].Rows)
                {

                    int classId = (int)row["ClassId"];
                    int classNumber = (int)row["ClassNumber"];
                    int seatsNumber = (int)row["SeatsNumber"];
                    bool active = (bool)row["Active"];
                    int insId = (int)row["InsId"];

                    Classroom c = new Classroom(classId, classNumber, seatsNumber, active, insId);
                    Classrooms.Add(c);
                }

            }
        }
        public static Classroom GetClassroomById(int classId)
        {
            foreach (Classroom classroom in Classrooms)
            {
                if (classroom.ClassId == classId)
                {
                    return classroom;
                }
            }
            return null;
        }
        public static Classroom GetClassroomByNumber(int classNumber)
        {
            foreach (Classroom classroom in Classrooms)
            {
                if (classroom.ClassNumber == classNumber)
                {
                    return classroom;
                }
            }
            return null;
        }
        public static Appointment GetAppointmentById(int Id)
        {
            foreach (Appointment appointment in Appointments)
            {
                if (appointment.Id == Id)
                {
                    return appointment;
                }
            }
            return null;
        }

        public static void ReadAppointments()
        {
            Appointments = new List<Appointment>();

            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from appointment where active = @active";
                command.Parameters.Add(new SqlParameter("active", true));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Appointment");
                foreach (DataRow row in ds.Tables["Appointment"].Rows)
                {
                    int id = (int)row["Id"];
                    bool active = (bool)row["Active"];
                    DateTime start = (DateTime)row["StartDate"];
                    DateTime end = (DateTime)row["EndDate"];
                    int classroomId = (int)row["ClassroomId"];
                    int teachingForm = (int)row["TeachingForm"];
                    int userId = (int)row["UserId"];

                    TeachingForm tf;

                    switch (teachingForm)
                    {
                        case 0: tf=TeachingForm.LECTURE; break;
                        case 1: tf = TeachingForm.PRACTISE; break;
                        default: tf = TeachingForm.UNSPECIFIED; ; break; 
                    }

                    Appointment a = new Appointment(id, active, start, end, classroomId, tf, userId);
                    Appointments.Add(a);                 
                }
            }
        }
    }
}



