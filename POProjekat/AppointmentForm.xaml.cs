﻿using POProjekat.Models;
using POProjekat.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace POProjekat
{
    /// <summary>
    /// Interaction logic for AppointmentForm.xaml
    /// </summary>
    public partial class AppointmentForm : Window
    {
        enum Status { ADD, EDIT }
        private Status _status;
        private Appointment selectedAppointment;
        public AppointmentForm(Appointment appointment)
        {
            List<int> classroomIdList = new List<int>();
            foreach (Classroom classroom in Data.Classrooms)
            {
                classroomIdList.Add(classroom.ClassId);
            }
            InitializeComponent();
            cmbType.ItemsSource = new List<TeachingForm>() { TeachingForm.LECTURE, TeachingForm.PRACTISE, TeachingForm.UNSPECIFIED};
            cmbClassId.ItemsSource = classroomIdList;
            if (appointment.Id != -1)
            {
                this._status = Status.EDIT;
            }
            else
            {
                this._status = Status.ADD;
            }
            selectedAppointment = appointment;
            this.DataContext = appointment;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Appointment app2;
  
            app2 = new Appointment(selectedAppointment.Active, selectedAppointment.StartDate, selectedAppointment.EndDate, selectedAppointment.ClassroomId, selectedAppointment.TeachingForm, selectedAppointment.UserId );
            
            if (_status.Equals(Status.ADD))
            {
                int id = app2.SaveAppointment();
                app2.Id = id;
                Data.Appointments.Add(app2);
            }
            if (_status.Equals(Status.EDIT))
            {
                app2.UpdateAppointment();
            }

            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
